+++
fragment = "hero2"
#disabled = true
date = "2016-09-07"
weight = 50
background = "light" # can influence the text color
particles = true
typed = true
frases_url =  '/data/frases.json'

title = "Gaita de Boto Aragonesa"
subtitle = "Recursos y documentación. En construcción"
minHeight = "90vh"

[header]
  image = "gastellu.jpg"
  minHeight = "90vh"


+++
