+++
fragment = "config"

[[config]]
  type = "css" # Acceptable values are icon, meta, link, css, js. Default is empty. Would not add anything on empty.
  block = true # If set to true, would inject the code to the <head> tag. Default is false
  html = """<style>
@media (max-width: 1400px) and (min-width: 1200px) {
    .titulo {
        font-size: 4rem;
        line-height: 6rem;
        font-weight: 900;
    }
    .letrastyped {
        font-size: 3rem;
        line-height: 4rem;
        font-weight: 900;
    }
    .vspace {
        min-height: 10vh;
    }
}
@media (max-width: 1200px) {
    .titulo {
        font-size: 2rem;
        line-height: 3rem;
        font-weight: 600;
    }
    .letrastyped {
        font-size: 1.6rem;
        line-height: 2.5rem;
        font-weight: 600;
    }
    .vspace {
        min-height: 10vh;
    }
}

.subratype {
    background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, #AF6679  80%); 
  }
.typed-cursor {
    color: #AF6679;
}

</style>
  """

+++